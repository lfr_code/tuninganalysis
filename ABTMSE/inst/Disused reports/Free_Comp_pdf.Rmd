
---
title: "Generic OM Sensitivity report"
subtitle: "ABT-MSE"
author: "Tom Carruthers"
date:  "`r format(Sys.time(), '%B %d, %Y')`"
output: 
  pdf_document:
    toc: true
    number_sections: true 
---


<style type="text/css">

body{ /* Normal  */
   font-size: 16px;
}
td {  /* Table  */
   font-size: 14px;
}
title { /* title */
 font-size: 26px;
}
h1 { /* Header 1 */
 font-size: 24px;
 color: DarkBlue;
}
h2 { /* Header 2 */
 font-size: 21px;
 color: DarkBlue;
}
h3 { /* Header 3 */
 font-size: 19px;
 color: DarkBlue;
}
code.r{ /* Code block */
  font-size: 16px;
}
pre { /* Code block */
  font-size: 16px
}
</style>

# Introduction / notes

```{r intro,echo=F,warning=F,error=F,message=F,results='asis'}
cat(introtext)

```



# Comparison of global likelihoods

```{r loadsecret,echo=F,warning=F,error=F,message=F}




outs<-OMIs<-new('list')
nOMs<-length(OMdirs)

Sres1<-Sres2<-NULL#array(NA,c(nOMs,8))
out<-M3read(OMdirs[1])
load(paste0(OMdirs[1],"/OMI"))
SSBstore<-array(0,c(nOMs,out$np,out$ny))
SSB0store<-array(0,c(nOMs,out$np))
SSBastore<-array(0,c(nOMs,out$np,out$ny))
ny<-out$ny
nHy<-out$nHy
np<-out$np
na<-out$na
dynB0<-array(NA,c(nOMs,out$np,nHy+ny))
B_BMSY_st<-array(NA,c(nOMs,out$np))



#sfInit(parallel=T,cpus=min(nOMs,8))
#M3read<-ABTMSE::M3read
#sfExport("M3read")

outread<-function(x,OMdirs)M3read(OMdirs[x])
OMIread<-function(x,OMdirs){
  load(paste0(OMdirs[x],"/OMI"))
  return(OMI)
}

for(i in 1:nOMs){
  
  out<-M3read(OMdirs[i])
  outs[[i]]<-out
  
  muSSB<-apply(out$SSB,1:2,mean) # by stock (below is by east-west area)
  Nind<-TEG(dim(out$N))
  SSB<-array(NA,dim(out$N))
  SSB[Nind]<-out$N[Nind]*out$mat_age[Nind[,c(1,4)]]*out$wt_age[Nind[,c(2,4,1)]]
  SSBsum<-apply(SSB,c(2,3,5),sum) # SSB by year and area in spawning season
  SSBmu<-apply(SSBsum,c(1,3),mean)
  #sum(SSBmu[55,])==sum(SSBsum[55,,])/4 # Debug test
  SSBastore[i,1,]<-apply(SSBmu*rep(c(0,0,0,1,1,1,1),each=out$ny),1,sum)
  SSBastore[i,2,]<-apply(SSBmu*rep(c(1,1,1,0,0,0,0),each=out$ny),1,sum)

  j<-strsplit(OMdirs[i],"/")
  j<-as.numeric(j[[1]][length(j[[1]])])
  #load(paste0(OMdirs[i],"/OM"))
  load(paste0(OMdirs[i],"/OMI"))
  OMIs[[i]]<-OMI
  
  #OMnames[i]<-paste(as.character(Design$Design_Ref[j,1]),
  #                  paste(as.character(Design$Design_Ref[j,2])),
  #                  paste(as.character(Design$Design_Ref[j,3])))
  
  VBi<-out$VB/rep(apply(out$VB,4,mean),each=out$ny*out$ns*out$nr)

  opt<-SRplot(out,years=OMI@years,type=OMI@SRtype,plot=F,OMI@SRminyr,OMI@SRmaxyr)

  R0s<-opt$R0s_now
  res<-MSYMLE(out,OMI)

  res[,c(2,3,6,7,8)]<-round(res[,c(2,3,6,7,8)],3)
  res[,c(1,4,5)]<-round(res[,c(1,4,5)]/1000000,2)
  SSBs<-round(out$SSB[,out$ny,out$ns]/1000,0)
  B_BMSY_st[i,]<-res[,6]
 
  SSBstore[i,,]<-apply(out$SSB,1:2,mean)/1000
  SSB0store[i,]<-out$SSB0/1000
  
  
  yrs<-OMI@years[1]:OMI@years[2]
  surv<-exp(-t(apply(cbind(c(0,0),OMI@Ma[,1:(OMI@na-1)]),1,cumsum)))
  surv[,OMI@na]<-surv[,OMI@na]+surv[,OMI@na]*exp(-OMI@Ma[,OMI@na])/(1-exp(-OMI@Ma[,OMI@na]))
  matwt<-t(out$wt_age[out$ny,,])*out$mat_age*surv
  SSBpR<-apply(matwt,1,sum)
  
 
  for(pp in 1:np){
    
    SSB=out$R0[pp,1]*SSBpR[pp]#OMI@SSBpR[pp]
    dynB0[i,pp,1:nHy]<-rep(SSB,nHy)
    N<-out$R0[pp,1]*surv[pp,]
    for(yy in 1:ny){
      Nplus<-N[na]*exp(-OMI@Ma[pp,na])
      for(aa in na:2)N[aa]<-N[aa-1]*exp(-OMI@Ma[pp,aa-1])
      N[1]<-out$R0[pp,yy]
      N[na]<-N[na]+Nplus
      dynB0[i,pp,nHy+yy]<-sum(out$wt_age[out$ny,,pp]*out$mat_age[pp,]*N)
    }
  }

  refyr<-2016-OMI@years[1]+1

  Fap<-meanFs(FML=out$FL[refyr,,,,], iALK=out$iALK[,refyr,,], N=out$N[,refyr,,,],
         wt_age=t(out$wt_age[refyr,,]))

  F_FMSY<-apply(Fap,1,max)/res[,2]
  SSB_SSB0<-apply(out$SSB[,refyr,],1,mean)/out$SSB0#res$SSBMSY
  res<-MSYMLE(out,OMI)
  res<-cbind(res,F_FMSY,SSB_SSB0)
  res[,c(2,3,6,7,8,9,10)]<-round(res[,c(2,3,6,7,8,9,10)],3)
  res[,c(1,4,5)]<-round(res[,c(1,4,5)]/1000,0)
  res[res[,2]==5,2]<-"5 (hit max bound)"
  BMSY<-round(res[,7]*dynB0[i,,nHy+ny]/1000,0)
  B_BMSY<-round(apply(out$SSB,1:2,mean)[,out$ny]/(BMSY*1000),3)

  Dep_dyn<-round(apply(out$SSB,1:2,mean)[,out$ny]/dynB0[i,,nHy+ny],3)
  res<-cbind(res[,c(1,2)],BMSY,res[,c(7,9)],B_BMSY,Dep_dyn)
  names(res)<-c("MSY","FMSY","BMSY","BMSY_B0","F_FMSY","B_BMSY","Dep")
  Sres1<-rbind(Sres1,c(OM=OMnos[i],Code=OMnames[i],res[1,]))
  Sres2<-rbind(Sres2,c(OM=OMnos[i],Code=OMnames[i],res[2,]))

}

Sres1<-as.data.frame(Sres1)
Sres2<-as.data.frame(Sres2)


#outs<-sfLapply(1:nOMs,outread,OMdirs=OMdirs)
#OMIs<-sfLapply(1:nOMs,OMIread,OMdirs=OMdirs)
#outs<-lapply(1:nOMs,outread,OMdirs=OMdirs)
#OMIs<-lapply(1:nOMs,OMIread,OMdirs=OMdirs)

tabs<-LHtabs(outs,OMIs,OMnos,OMnams=OMnames)
LHs<-tabs$LHs
LHsU<-tabs$LHsU
wts_tab<-tabs$wts_tab

colr<-function(x,ncol=1000,cond=NA){
  if(sd(x)==0){
    tempcol=rep('black',length(x))
  }else{  
    if(!is.na(cond[1]))x[cond]<-mean(x[!cond])
    xconv<-1-((x-min(x,na.rm=T))/(max(x,na.rm=T)-min(x,na.rm=T)))
    tempcol<-hsv(h=xconv*0.33)
    if(!is.na(cond[1]))tempcol[cond]="black"
  }
  tempcol
}

library(dplyr)
library(kableExtra)




```



## Eastern stock

Table 1. Reference points for the Eastern stock using 2015 stock-recruitment (R0 and steepness). FMSY refers to apical F (the instantaneous fishing mortality rate on the most selected length class). UMSY is current yield divided by vulnerable biomass. Tabulated biomass numbers (BMSY, BMSY_B0 and B2015) refer to spawning biomass. These biomass numbers and MSY numbers are expressed in thousands of tonnes. Depletion is spawning biomass in 2015 relative to the 'dynamic B0' (the spawning biomass under zero fishing accounting for shifts in recruitment). 

```{r refpoints_Et, results='asis',echo=FALSE,fig.width=5}
Sres1%>%
 kable(format = "markdown", escape = F) %>%
  kable_styling("striped", full_width = T) 
#kable(Sres1)

```


## Western stock

Table 2. As Table 1 but for the Western stock. 

```{r refpoints_W, results='asis',echo=FALSE,fig.width=5}

Sres2%>%
 kable(format = "markdown", escape = F) %>%
  kable_styling("striped", full_width = T)
  
```



Table 3. These are weighted negative log-likelihoods. Color coding is by column. Rows with all black values depict models that did not converge. 

```{r TotLHF, fig.width=11, fig.height=9,echo=FALSE}

LHs%>%
  mutate(
    OM=cell_spec(OM,"markdown",bold=T),
    Code=cell_spec(Code,"markdown",bold=T),
    Cat=cell_spec(Cat,"markdown",color=colr(LHs$Cat),bold=T),
    CR=cell_spec(CR,"markdown",color=colr(LHs$CR),bold=T),
    Surv=cell_spec(Surv,"markdown",color=colr(LHs$Surv),bold=T),
    Comp=cell_spec(Comp,"markdown",color=colr(LHs$Comp),bold=T),
    SOOm=cell_spec(SOOm,"markdown",color=colr(LHs$SOOm),bold=T),
    SOOg=cell_spec(SOOg,"markdown",color=colr(LHs$SOOg),bold=T),
    Tag=cell_spec(Tag,"markdown",color=colr(LHs$Tag),bold=T),
    Rec=cell_spec(Rec,"markdown",color=colr(LHs$Rec),bold=T),
    Mov=cell_spec(Mov,"markdown",color=colr(LHs$Mov),bold=T),
    Sel=cell_spec(Sel,"markdown",color=colr(LHs$Sel),bold=T),
    SRA=cell_spec(SRA,"markdown",color=colr(LHs$SRA),bold=T),
    R0diff=cell_spec(R0diff,"markdown",color=colr(LHs$R0diff),bold=T),
    MI=cell_spec(MI,"markdown",color=colr(LHs$MI),bold=T),
    SPr=cell_spec(SPr,"markdown",color=colr(LHs$SPr),bold=T),
    TOT_nP=cell_spec(TOT_nP,"markdown",color=colr(LHs$TOT_nP),bold=T),
    TOT=cell_spec(TOT,"markdown",color=colr(LHs$TOT),bold=T)
    
  )%>%
  select(OM, Code,Cat,CR,Surv,Comp,SOOm,SOOg,Tag,Rec,Mov,Sel,SRA,R0diff,MI,SPr,TOT_nP,TOT) %>%
  kable(format = "markdown", escape = F) %>%
  kable_styling("striped")#, full_width = T) 


```
Cat = catch data by fleet, quarter and area. CR = the fishery dependent catch rate (CPUE) indices, Surv = fishery-independent survey indices, Comp = length composition data, SOOm = stock of origin microchemistry data, SOOg = stock of origin genetics, Tag = Electronic tagging data, Rec = prior on recruitment deviations, Mov = prior on movement parameters, Sel = prior on size selectivity parameters, SRA = penalty incurred when catches exceed F=1 catches in the stock reduction analysis phase (1864-1964), MI = a prior on similarity to the 'Master Index' that predicts F by year, area, season and fleet, R0diff = a prior on the difference in R0 estimated in two-phase recruitment models (recruitment level 1 and 3), SPr = seasonal distribution prior, TOT_nP = total global objective function without priors, TOT = total global objective function. 

Table 4. These are weighted negative log-likelihoods expressed as differences from the base-case weighting. Color coding is by column. Rows with all black values depict models that did not converge. 


```{r TotLHFconv, fig.width=9, fig.height=9,echo=FALSE}


for(i in 3:17)LHs[,i]<-round(LHs[,i]-LHs[1,i],1)

LHs%>%
  mutate(
    OM=cell_spec(OM,"markdown",bold=T),
    Code=cell_spec(Code,"markdown",bold=T),
    Cat=cell_spec(Cat,"markdown",color=colr(LHs$Cat),bold=T),
    CR=cell_spec(CR,"markdown",color=colr(LHs$CR),bold=T),
    Surv=cell_spec(Surv,"markdown",color=colr(LHs$Surv),bold=T),
    Comp=cell_spec(Comp,"markdown",color=colr(LHs$Comp),bold=T),
    SOOm=cell_spec(SOOm,"markdown",color=colr(LHs$SOOm),bold=T),
    SOOg=cell_spec(SOOg,"markdown",color=colr(LHs$SOOg),bold=T),
    Tag=cell_spec(Tag,"markdown",color=colr(LHs$Tag),bold=T),
    Rec=cell_spec(Rec,"markdown",color=colr(LHs$Rec),bold=T),
    Mov=cell_spec(Mov,"markdown",color=colr(LHs$Mov),bold=T),
    Sel=cell_spec(Sel,"markdown",color=colr(LHs$Sel),bold=T),
    SRA=cell_spec(SRA,"markdown",color=colr(LHs$SRA),bold=T),
    R0diff=cell_spec(R0diff,"markdown",color=colr(LHs$R0diff),bold=T),
    MI=cell_spec(MI,"markdown",color=colr(LHs$MI),bold=T),
    SPr=cell_spec(SPr,"markdown",color=colr(LHs$SPr),bold=T),
    TOT_nP=cell_spec(TOT_nP,"markdown",color=colr(LHs$TOT_nP),bold=T),
    TOT=cell_spec(TOT,"markdown",color=colr(LHs$TOT),bold=T)
    
  )%>%
  select(OM, Code,Cat,CR,Surv,Comp,SOOm,SOOg,Tag,Rec,Mov,Sel,SRA,R0diff,MI,SPr,TOT_nP,TOT) %>%
  kable(format = "markdown", escape = F) %>%
  kable_styling("striped")#, full_width = T) 


```



Table 5. Weightings of likelihood components. 


```{r LHwt, fig.width=9, fig.height=9,echo=FALSE}



wts_tab%>%
kable(format = "markdown", escape = F) %>%
  kable_styling("striped")#, full_width = T)


```




# Comparison with 2017 Stock Assessments


```{r 2017comp, fig.width=7.5, fig.height=5,echo=FALSE}

ny<-outs[[1]]$ny
nHy<-outs[[1]]$nHy
np<-outs[[1]]$np
ns<-outs[[1]]$ns
nr<-outs[[1]]$nr
nf<-outs[[1]]$nf
nl<-outs[[1]]$nl
na<-outs[[1]]$na
cols<-rep(c('black','grey','blue','lightblue','pink'),2)
ltys<-rep(c(1,2),each=5)
muSSB<-new('list')
ylim<-new('list')
ylim[[1]]<-ylim[[2]]<-c(0,-1000)

for(OM in 1:nOMs){
  Nind<-TEG(dim(outs[[OM]]$N))
  SSB<-array(NA,dim(outs[[OM]]$N))
  SSB[Nind]<-outs[[OM]]$N[Nind]*outs[[OM]]$mat_age[Nind[,c(1,4)]]*outs[[OM]]$wt_age[Nind[,c(2,4,1)]]
  SSBsum<-apply(SSB,c(2,3,5),sum) # SSB by year and area in spawning season
  SSBmu<-apply(SSBsum,c(1,3),mean)
  #sum(SSBmu[55,])==sum(SSBsum[55,,])/4 # Debug test
  muSSB[[OM]]<-array(NA,c(np,ny))
  muSSB[[OM]][1,]<-apply(SSBmu*rep(c(0,0,0,1,1,1,1),each=outs[[OM]]$ny),1,sum)
  muSSB[[OM]][2,]<-apply(SSBmu*rep(c(1,1,1,0,0,0,0),each=outs[[OM]]$ny),1,sum)
  snams<-c("East","West")
  stocknames<-c("East area","West area","All Atlantic")
  if(max(muSSB[[OM]][1,])>ylim[[1]][2])ylim[[1]][2]<-max(muSSB[[OM]][1,])
  if(max(muSSB[[OM]][2,])>ylim[[2]][2])ylim[[2]][2]<-max(muSSB[[OM]][2,])
}

yrs<-OMIs[[1]]@years[1]:OMIs[[1]]@years[2]
par(mfcol=c(1,2),mai=c(0.35,0.35,0.01,0.1),omi=c(0.4,0.4,0.4,0.05))

for(pp in np:1){
  
  # SSB 
  tdat<-subset(dat,dat$area==snams[pp])[,c(2:4)]
  names(tdat)<-c("Assessment","Year","Val")
  
  plot(yrs,muSSB[[1]][pp,]/1E6,lwd=2,ylab="",type="l",ylim=ylim[[pp]]/1E6,yaxs='i',col=cols[1],lty=ltys[1])
  for(i in 2:nOMs)lines(yrs,muSSB[[i]][pp,]/1E6,lwd=2,col=cols[i],lty=ltys[i])
  cond<-tdat$Assessment=="SS"
  lines(tdat$Year[cond],as.numeric(as.character(tdat$Val[cond]))/1000,col="#ff000090",lwd=2)
  lines(tdat$Year[!cond],as.numeric(as.character(tdat$Val[!cond]))/1000,col="#00ff0090",lwd=2)
  mtext(stocknames[pp],3,line=1,font=2)
  if(pp==np)mtext("SSB(kt)",2,line=2.8,font=2)

  if(pp==2)legend('top',legend=c("M3","SS","VPA"),cex=0.75,text.col=c('black','red','green'),text.font=2,bty='n')
  if(pp==1)legend('top',legend=LHs[,2],cex=0.75,text.col=cols,lty=ltys,col=cols,text.font=2,bty='n')
}

mtext("Year",1,line=0.5,font=2,outer=T)

```

Figure 1. Regional spawning stock comparisons (by area, divisible by 45deg W) with 2017 stock assessment. Note that annual estimates from the operating model are calculated from average of the seasonal predictions. 



# Spawning Stock Biomass


```{r SSB, fig.width=8.5, fig.height=9,echo=FALSE}

F_E_E<-F_E_W<-F_W_E<-F_W_W<-F_EinW<-F_WinE<-SSB_W<-SSB_E<-D_W<-D_E<-new('list')
ind<-TEG(dim(outs[[1]]$N))
indw<-ind[,c(2,4,1)]

for(OM in 1:nOMs){
  
  B<-SSB<-array(NA,dim(outs[[OM]]$N))
 
  B[ind]<-outs[[OM]]$N[ind]*outs[[OM]]$wt_age[indw]
  SSB[Nind]<-outs[[OM]]$N[ind]*outs[[OM]]$mat_age[ind[,c(1,4)]]*outs[[OM]]$wt_age[ind[,c(2,4,1)]]
  
  #RAIp<-apply(B,c(5,3,2),sum) # r s y
  #muRAIp<-mean(RAIp)
  RAIpP<-apply(B,c(5,3,2,1),sum) # r s y p
  #RAIpP<-RAIpP/muRAIp
  #SSBpP<-apply(SSB,c(5,3,2,1),sum)# r s y p
    
  Wareas<-1:3
  Eareas<-4:7
  
  EinW<-apply(RAIpP[Wareas,,,1],3,sum)
  EinE<-apply(RAIpP[Eareas,,,1],3,sum)
  WinW<-apply(RAIpP[Wareas,,,2],3,sum)
  WinE<-apply(RAIpP[Eareas,,,2],3,sum)
  
  SSB_E[[OM]]<-apply(SSB[1,,2,,],1,sum)/1E6 #  y spawning season = 2
  SSB_W[[OM]]<-apply(SSB[2,,2,,],1,sum)/1E6 #  y spawning season = 2
 
  D_E[[OM]]<- SSB_E[[OM]]/SSB_E[[OM]][1]
  D_W[[OM]]<- SSB_W[[OM]]/SSB_W[[OM]][1]
  
  F_EinW[[OM]]<-EinW/(EinE+EinW)
  F_WinE[[OM]]<-WinE/(WinW+WinE)
  
  F_E_E[[OM]]<-EinE/(EinE+WinE)
  F_E_W[[OM]]<-EinW/(EinW+WinW)
  F_W_E[[OM]]<-WinE/(EinE+WinE)
  F_W_W[[OM]]<-WinW/(EinW+WinW)
 
}


ylim_SSB_W<-range(lapply(SSB_W,range))
ylim_SSB_E<-range(lapply(SSB_E,range))
ylim_D_W<-range(lapply(D_W,range))
ylim_D_E<-range(lapply(D_E,range))


par(mfrow=c(2,2),mai=c(0.35,0.35,0.4,0.01),omi=c(0.4,0.4,0.05,0.05))
yrs<-OMIs[[1]]@years[1]:OMIs[[1]]@years[2]

plot(yrs,SSB_W[[1]],col="white",ylim=c(0,ylim_SSB_W[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,SSB_W[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("West stock SSB",3,line=1,font=2)

legend('topright',legend=LHs[,2],cex=1,text.col=cols,lty=ltys,col=cols,text.font=2,bty='n')

plot(yrs,SSB_E[[1]],col="white",ylim=c(0,ylim_SSB_E[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,SSB_E[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("East stock SSB",3,line=1,font=2)

plot(yrs,D_W[[1]],col="white",ylim=c(0,ylim_D_W[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,D_W[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("West stock SSB trend",3,line=1,font=2)

plot(yrs,D_E[[1]],col="white",ylim=c(0,ylim_D_E[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,D_E[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("East stock SSB trend",3,line=1,font=2)



```


Figure 2. Stock-specific spawning biomass in kilo tonnes (top row) and relative to 1952 (bottom row)



# Stock mixing 

```{r mxing, fig.width=8.5, fig.height=9,echo=FALSE}


ylim_E_E<-range(lapply(F_E_E,range))
ylim_E_W<-range(lapply(F_E_W,range))
ylim_W_E<-range(lapply(F_W_E,range))
ylim_W_W<-range(lapply(F_W_W,range))
ylim_EinW<-range(lapply(F_EinW,range))
ylim_WinE<-range(lapply(F_WinE,range))

yrs<-OMIs[[1]]@years[1]:OMIs[[1]]@years[2]
par(mfrow=c(2,2),mai=c(0.35,0.35,0.4,0.01),omi=c(0.4,0.4,0.05,0.05))

plot(yrs,F_EinW[[1]],col="white",ylim=c(0,ylim_EinW[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,F_EinW[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("Eastern stock in the West area",3,line=1,font=2)

plot(yrs,F_WinE[[1]],col="white",ylim=c(0,ylim_WinE[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,F_WinE[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("Western stock in the East area",3,line=1,font=2)


plot(yrs,F_E_W[[1]],col="white",ylim=c(0,ylim_E_W[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,F_E_W[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("Western area biomass that is Eastern",3,line=1,font=2)

plot(yrs,F_W_E[[1]],col="white",ylim=c(0,ylim_W_E[2]),yaxs='i')
for(i in 1:nOMs)lines(yrs,F_W_E[[i]],col=cols[i],lty=ltys[i],lwd=2)
mtext("Eastern area biomass that is Western",3,line=1,font=2)

legend('topright',legend=LHs[,2],cex=1,text.col=cols,lty=ltys,col=cols,text.font=2,bty='n')
mtext("Biomass fraction",2,line=0.5,font=2,outer=T)
mtext("Year",1,line=0.5,font=2,outer=T)

```

Figure 3. Biomass fractions by stock (top row) and area (bottom row).  
